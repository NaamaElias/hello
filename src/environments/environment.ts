// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCou_rGVqx0qQtMilgukiGO8bahEfkJ6os",
    authDomain: "hello-naamael.firebaseapp.com",
    databaseURL: "https://hello-naamael.firebaseio.com",
    projectId: "hello-naamael",
    storageBucket: "hello-naamael.appspot.com",
    messagingSenderId: "887455722953",
    appId: "1:887455722953:web:eec21cb0fad1ded548cc18"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
