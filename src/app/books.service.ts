import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"jhskjhakshd"},
{title:'War and Peace', author:'Leo Tolstoy',summary:"sqlK;Lslmd"}, 
{title:'The Magic Mountain', author:'Thomas Mann',summary:"dhjkdkjL"}];

/*
public getBooks(){
  const booksObservable = new Observable(observer =>{
    setInterval(()=>observer.next(this.books),500)
  });
  return booksObservable;
}
*/
/*
public getBooks(){
  return this.books;
}
*/

  bookCollection:AngularFirestoreCollection; 
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  /*
  public getBooks(userId){
    this.bookCollection = this.db.collection(`users/${userId}/books`); 
    return this.bookCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id;
          return data; 
        }
      )
    ))
    
  }
  */

 public getBooks(userId,startAfter){
  this.bookCollection = this.db.collection(`users/${userId}/books`,
  ref => ref.orderBy('title','asc').limit(4).startAfter(startAfter)); 
  return this.bookCollection.snapshotChanges()
}

  deleteBook(Userid:string, id:string){
    this.db.doc(`users/${Userid}/books/${id}`).delete();
  }

  addBook(userId:string,title:string,author:string){
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection('books').add(book);
  }
 
  updateBook(userId:string,id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        title:title,
        author:author
      }
    );
  }

  // getBooks(userId):Observable<any[]>{

  // }
  constructor(private db:AngularFirestore) { }
}
