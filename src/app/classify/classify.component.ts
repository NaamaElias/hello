import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  
  networks: string[] = ['BBC', 'CNN', 'NBC'];

  selectedNetwork;
  constructor(private route:ActivatedRoute) { }
  ngOnInit(): void {
    this.selectedNetwork = this.route.snapshot.params.network;
  }

}